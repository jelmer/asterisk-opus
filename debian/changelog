asterisk-opus (13.7+20161113-4) unstable; urgency=medium

  * Modernize cdbs:
    + Do copyright-check in maintainer script (not during build).
      Stop build-depend on licensecheck.
  * Modernize Vcs-* fields: Use git (not cgit) in path.
  * Declare compliance with Debian Policy 4.1.0.
  * Update copyright info:
    + Use https protocol in file format URL.
    + Extend coverage for myself.
  * Advertise DEP-3 format in patch headers.
  * This (simply rebuilding) fixes linking with current Asterisk.
    Closes: Bug#872760. Thanks to Sam Hartman.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 28 Aug 2017 10:32:42 +0200

asterisk-opus (13.7+20161113-3) unstable; urgency=medium

  * Really avoid res_format module when Asterisk links against Opus.
  * Add git-buildpackage config:
    + Use pristine-tar.
    + Sign tags.
    + Filter any .git* file.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 22 Dec 2016 01:20:17 +0100

asterisk-opus (13.7+20161113-2) unstable; urgency=medium

  * Update copyright info: Drop bogus entry.
    Thanks to Thorsten Alteholz.
  * Avoid res_format module when Asterisk links against Opus.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 21 Dec 2016 15:07:27 +0100

asterisk-opus (13.7+20161113-1) unstable; urgency=low

  * Initial release.
    Closes: bug#848560.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 18 Dec 2016 13:03:07 +0100
